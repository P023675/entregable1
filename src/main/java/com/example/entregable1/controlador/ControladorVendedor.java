package com.example.entregable1.controlador;

import com.example.entregable1.modelo.Producto;
import com.example.entregable1.modelo.Vendedor;
import com.example.entregable1.servicio.ServicioProducto;
import com.example.entregable1.servicio.ServicioVendedor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.v1}/vendedores/{idVendedor}")
public class ControladorVendedor {

    @Autowired
    ServicioVendedor servicioVendedor;

    @GetMapping
    public ResponseEntity<Vendedor> obtenerVendedor(@PathVariable long idVendedor){
        try{
            return ResponseEntity.ok(buscarVendedorPorId(idVendedor));
        }catch (ResponseStatusException x){
            return ResponseEntity.notFound().build();
        }
    }


    public static class VendedorEntrada{
        public String nombre;
        public String direccion;
        public Double sueldo;
    }

    @PutMapping
    public void reemplazarVendedor(@PathVariable(name = "idVendedor") long id,
                                   @RequestBody VendedorEntrada v){

        final Vendedor x = buscarVendedorPorId(id);

        if(v.nombre == null || v.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(v.direccion == null || v.direccion.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(v.sueldo == null || v.sueldo <= 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.nombre = v.nombre;
        x.direccion = v.direccion;
        x.sueldo = v.sueldo;

        this.servicioVendedor.reemplazarVendedor(id,x);
    }

    @PatchMapping
    public void modificarVendedor(@PathVariable long idVendedor,
                                  @RequestBody VendedorEntrada v){
        final Vendedor x = buscarVendedorPorId(idVendedor);
        if(v.nombre != null){
            if(v.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.nombre = v.nombre;
        }
        if(v.direccion != null) {
            if(v.direccion.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.direccion = v.direccion;
        }
        if(v.sueldo != null){
            if(v.sueldo <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.sueldo = v.sueldo;
        }

        this.servicioVendedor.reemplazarVendedor(idVendedor,x);
    }

    @DeleteMapping
    public void borrarVendedor(@PathVariable(name  = "idVendedor") long id){
        this.servicioVendedor.eliminarVendedor(id);
    }

    private Vendedor buscarVendedorPorId(long idVendedor){
        final Vendedor v = this.servicioVendedor.obtenerVendedor(idVendedor);
        if(v == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return v;
    }

}
