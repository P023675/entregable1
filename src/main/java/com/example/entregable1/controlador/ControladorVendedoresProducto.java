package com.example.entregable1.controlador;

import com.example.entregable1.modelo.Producto;
import com.example.entregable1.modelo.Vendedor;
import com.example.entregable1.servicio.ServicioProducto;
import com.example.entregable1.servicio.ServicioVendedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.beans.PropertyVetoException;
import java.util.List;

@RestController
@RequestMapping("${api.v1}/productos/{idProducto}/vendedores")
public class ControladorVendedoresProducto {

    @Autowired
    ServicioProducto servicioProducto;
    @Autowired
    ServicioVendedor servicioVendedor;

    @GetMapping
    public List<Long> obtenerIdsVendedores(@PathVariable long idProducto){
        final Producto p = obtenerProductoPorId(idProducto);
        return p.idsVendedores;
    }

    public static class VendedorEntrada{
        public long idVendedor;
    }

    @DeleteMapping("/{idVendedor}")
    public void eliminarReferenciaVendedorProducto(@PathVariable long idProducto,
                                                   @PathVariable long idVendedor){

        final Producto x = obtenerProductoPorId(idProducto);

        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i< x.idsVendedores.size(); i++){
            if(x.idsVendedores.get(i) == idVendedor){
                j = i;
                encontrado = true;
            }
        }

        if(encontrado){
            x.idsVendedores.remove(j);
            this.servicioProducto.reemplazarProducto(idProducto,x);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarReferenciaVendedorProducto(@PathVariable long idProducto,
                                                  @RequestBody VendedorEntrada v){

        final Producto x = obtenerProductoPorId(idProducto);
        final Vendedor y = this.servicioVendedor.obtenerVendedor(v.idVendedor);

        if(y==null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for (long id: x.idsVendedores){
            if(id== v.idVendedor)
                return; // 200 OK
        }

        x.idsVendedores.add(v.idVendedor);
        this.servicioProducto.reemplazarProducto(idProducto,x);
    }

    public Producto obtenerProductoPorId(Long idProducto){
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

}
