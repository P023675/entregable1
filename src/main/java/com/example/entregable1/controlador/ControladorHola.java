package com.example.entregable1.controlador;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/saludo")
public class ControladorHola {

    private String saludo = "Hola";

    //@RequestMapping(method = RequestMethod.GET, path ="/hola")
    @GetMapping
    public String saludar(@RequestParam(required = false, name = "persona") String nombre){
        if(nombre == null || nombre.trim().isEmpty())
            return String.format("%s Mundo", this.saludo);

        return String.format("%s %s", this.saludo, nombre);
    }

    @PutMapping
    //public ResponseEntity guardarSaludoNuevo(@RequestParam(name = "saludo") String saludoNuevo){
    public ResponseEntity guardarSaludoNuevo(@RequestBody String saludoNuevo){
        if(saludoNuevo!= null && !saludoNuevo.trim().isEmpty()){
            this.saludo = saludoNuevo.trim();
            return ResponseEntity.ok("Se guardó el saludo: " + saludoNuevo);
        } else  {
            return ResponseEntity.status(403).body("El saludo no puede estar vacío.");
            //throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El saludo no puede estar vacío.");
        }
    }
}
