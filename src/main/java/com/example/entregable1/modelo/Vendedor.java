package com.example.entregable1.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Vendedor {
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public long id;

    public String nombre;
    public String direccion;
    public double sueldo;
}
