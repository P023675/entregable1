package com.example.entregable1.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Producto {

    public long id;
    public String nombre;
    public double precio;
    public double cantidad;

    // Jackson - Serializador/Deserializador JSON usado por Spring
    @JsonIgnore
    public final List<Long> idsVendedores= new ArrayList<>();
}
