package com.example.entregable1.servicio;

import com.example.entregable1.modelo.Vendedor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioVendedor {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long , Vendedor> vendedores = new ConcurrentHashMap<Long,Vendedor>();

    public long agregarVendedor(Vendedor v){
        v.id = this.secuenciador.incrementAndGet();
        this.vendedores.put(v.id,v);
        return v.id;
    }

    public Vendedor obtenerVendedor(long id){
        final Vendedor v = this.vendedores.get(id);
        return v;
    }

    public List<Vendedor> obtenerVendedores(){
        return this.vendedores
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    public void reemplazarVendedor(long id, Vendedor v){
        // FIXME: verificar que exista el vendedor con este id.
        v.id = id;
        this.vendedores.put(id,v);
    }

    public void eliminarVendedor(long id){
        this.vendedores.remove(id);
    }

}
